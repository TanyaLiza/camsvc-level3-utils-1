package com.ferguson.service.level3;

import java.io.File;
import java.net.HttpURLConnection;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.properties.PropertiesComponent;

import com.ferguson.service.common.camel.CommonConstants;
import com.ferguson.service.common.camel.policy.LoggingRoutePolicyFactory;

@ContextName("com.ferguson.service.level3")
public class CamelRoute extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        final PropertiesComponent pc = new PropertiesComponent();
        pc.setLocation("file:${jboss.server.config.dir}" + File.separator + "FEI-Props" + File.separator + "level3-utils.properties");
        getContext().addComponent("properties", pc);

        getContext().addRoutePolicyFactory(new LoggingRoutePolicyFactory());

        onException(Throwable.class).handled(true)
                .setProperty(CommonConstants.FEI_RESP_CODE_PROP, constant(Constants.ERROR_PROCESSING_REQUEST))
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_INTERNAL_ERROR))
                .bean("responseBuilder");

    }
}