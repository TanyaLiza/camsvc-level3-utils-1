package com.ferguson.service.level3;

import java.net.HttpURLConnection;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.bean.validator.BeanValidationException;
import org.apache.camel.model.dataformat.JsonLibrary;

import com.fasterxml.jackson.core.JsonParseException;
import com.ferguson.service.common.camel.CommonConstants;
import com.ferguson.service.common.camel.util.Helpers;

@ContextName("com.ferguson.service.level3")
public class AddProducerCamelRoute extends RouteBuilder {
	@Override
	public void configure() throws Exception {
		onException(JsonParseException.class).handled(true)
				.setProperty(CommonConstants.FEI_RESP_CODE_PROP, constant(Constants.BAD_REQUEST))
				.setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP,
						constant(Helpers.translateMessageCode(Constants.BAD_REQUEST)))
				.end();

		onException(Throwable.class).handled(true)
				.setProperty(CommonConstants.FEI_RESP_CODE_PROP,
						constant(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()))
				.setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP,
						constant(Helpers.translateMessageCode(Constants.ERROR_PROCESSING_REQUEST)))
				.end();

		from(ConstantsRest.ROUTE_ID_ADDAMQPRODUCER).routeId("addAmqProducer")
				.bean("addProducerProcessor", "checkTransId")
				               
				.onException(BeanValidationException.class).id("Bean Validation Excp").handled(true)
				.process("beanValidationExceptionProcessor").marshal().json(JsonLibrary.Jackson).end()
				.to("bean-validator://requestValidator")
				
				.process("addProducerProcessor")
				.id("Create a Publisher to topic/queue").log("queueName : ${headers.route.id}")
				.setExchangePattern(ExchangePattern.OutOnly)
				.setProperty(CommonConstants.FEI_LOG_MESSAGE_PROP,
						simple("Successfully added Producer for amq address ${headers.route.id}"))
				.bean("responseBuilder").setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpURLConnection.HTTP_OK));
	}
}